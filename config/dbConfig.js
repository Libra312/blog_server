const config = {
  // 数据库配置
  mysql: {
    DATABASE: 'libra',
    USERNAME: 'root',
    PASSWORD: 'password',
    PORT: '3306',
    HOST: 'localhost'
  },
  redis:{
    host: '127.0.0.1',
    port: '6379',
    password: 'lixin521'
  }
}

module.exports = config