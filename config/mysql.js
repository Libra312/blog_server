let Sequelize = require('sequelize')
let config = require('./dbConfig.js')
let sequelize = new Sequelize(config.mysql.DATABASE, config.mysql.USERNAME, config.mysql.PASSWORD, {
  host:config.mysql.HOST,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})
module.exports = sequelize