const sequelize = require('../config/mysql')
const Redis = require('koa-redis')
const {valiEmail, valiName, addUser} = require('../service/user')
let Store = new Redis().client
let register = async (ctx, next) => {
  let {userEmail, userName, userPwd, userCode} = ctx.request.body
   // 查找数据库看用户是否存在
   let isEmail = await valiEmail(userEmail)
   if (isEmail.length) {
     ctx.body = {
       resultCode: -1,
       message: '邮箱已经被注册'
     }
     return
   }
   let isUserName = await valiName(userName)
   if (isUserName.length) {
     ctx.body = {
       resultCode: -1,
       message: '用户名被占用'
     }
     return
   }
  // 验证码验证
  if (userCode) {
    let saveCode = await Store.hget(`liMail${userName}`, 'userCode')
    let saveExpire = await Store.hget(`liMail${userName}`, 'expire')
    console.log()
    if (saveCode === userCode) {
      if (new Date().getTime() - saveExpire > 0) {
        ctx.body = {
          resultCode: -1,
          message: '验证码已经过期，请重新尝试'
        }
        return
      } 
    } else {
      ctx.body = {
        resultCode: -1,
        message: '请输入正确的验证码'
      }
      return
    }
  } else {
    ctx.body = {
      resultCode: -1,
      message: '请填写验证码'
    }
    return
  }
  let res = await addUser(ctx, next)
  if (res) {
    ctx.body = {
      resultCode: 0,
      message: '注册成功'
    }
  } else {
    ctx.body = {
      resultCode: 0,
      message: '注册失败'
    }
  }
}
let login = async (ctx, next) => {

}
module.exports = {
  register
}