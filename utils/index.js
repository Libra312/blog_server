const generatorRandom = (n, left, right) => {
  if (left >= right) {
    [left, right] = [right, left]
  }
  left = Math.floor(left)
  right = Math.floor(right)
  let arr = Array.of(n)
  for (let i = 0; i < n; i++) {
    arr[i] = Math.floor(Math.random() * (right - left + 1) + left)
  }
  return arr
}
const getCode = () => {
  return Math.random().toString(16).slice(2, 6).toUpperCase()
}
const getExpire = () => {
  return new Date().getTime()*60*60*1000
}
module.exports = {
  generatorRandom,
  getCode,
  getExpire
}