const user = require('../model/user.js')
async function valiEmail (userEmail) {
  return await user.findAll({
    where: {
      userEmail: userEmail
    }
  })
}
async function valiName (userName) {
  return await user.findAll({
    where: {
      userName: userName
    }
  })
}
async function addUser (ctx, next) {
  let {userEmail, userName, userPwd} = ctx.request.body
  return await user.create({
    userEmail,
    userName,
    userPwd
  })
}
module.exports = {
  valiEmail,
  valiName,
  addUser
}