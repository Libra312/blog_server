const router = require('koa-router')()
const {getCode} = require('../utils/index.js')
const send = require('../utils/email')
const {register} = require('../controller/user')
const user = require('../model/user')
router.prefix('/users')

router.post('/login', (ctx, next) => {
})

router.post('/register', async (ctx, next) => {
  await register(ctx, next)
})

router.get('/email', (ctx, next) => {
  let {userEmail} = ctx.request.query
  let code = getCode()
  let mailOptions = {
    from: '"Libra"<libra_085925@163.com>',
    to: userEmail,
    subject: '欢迎注册Libra', 
    html: `<div style="width:200px;height:200px;text-align:center;">
    <div>感谢您注册Libra</div>
    <div>下面是您的验证码</div>
    <div><strong>${code}</strong></div>
  </div>`
  };
  send(ctx, code, mailOptions)
  ctx.body={
    resultCode: 0,
    message: '验证码已发送，有效期为一分钟'
  }
})

module.exports = router
