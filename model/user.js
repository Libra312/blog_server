const Sequelize = require('sequelize')
const sequelize = require('../config/mysql')

const User = sequelize.define('user', {
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  userEmail: {
    type: Sequelize.STRING,
    allowNull: false,
    validate:{
      isEmail: true,   //类型检测,是否是邮箱格式
    }
  },
  userName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  userPwd: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  //使用自定义表名
  freezeTableName: true
})
//先删除后同步
User.sync({
  force: false
})
module.exports = User